export const state = () => ({
  extensionSuccessMessage: null,
  enableProjectDefaults: false,
  projectExtension: {
    name: '',
    seed: 0,
    unrolling: {
      count: 0,
      precision: 0,
    },
    type_representatives: [],
    system_default: {
      objectives: [],
      cv: 0,
      default_job_extension: {
        tr: '',
        mode: {
          duration: {
            type: '',
            cv: 0,
            number: 0,
          },
        },
      },
    },
    global_resources: [],
    projects: [],
  },
  single_objective: {
    type: '',
    function: '',
  },
  kolischList: [],
  addedProject: {
    project: 0,
    filename: '',
    project_default: {
      objective: {
        type: '',
        function: '',
      },
      cv: 0,
      default_job_extension: {
        tr: '',
      },
    },
    job_extensions: [],
    resource_extensions: [],
  },
})

export const mutations = {
  updateName(state, payload) {
    state.projectExtension.name = payload
  },
  updateSeed(state, payload) {
    state.projectExtension.seed = parseInt(payload)
  },
  updateUnrollingCount(state, payload) {
    state.projectExtension.unrolling.count = parseInt(payload)
  },
  updateUnrollingPrecision(state, payload) {
    state.projectExtension.unrolling.precision = parseFloat(payload)
  },
  addTR(state, payload) {
    state.projectExtension.type_representatives.push({ ...payload })
  },
  updateSysDefaultDataCV(state, payload) {
    state.projectExtension.system_default.cv = parseFloat(payload)
  },
  updateSysDefaultDataTR(state, payload) {
    state.projectExtension.system_default.default_job_extension.tr = payload
  },
  updateSysDefaultDataModeDurationType(state, payload) {
    state.projectExtension.system_default.default_job_extension.mode.duration.type =
      payload
  },
  updateSysDefaultDataModeDurationCV(state, payload) {
    state.projectExtension.system_default.default_job_extension.mode.duration.cv =
      parseFloat(payload)
  },
  updateSysDefaultDataModeDurationNumber(state, payload) {
    state.projectExtension.system_default.default_job_extension.mode.duration.number =
      parseInt(payload)
  },
  updateSysDefaultData(state, payload) {
    state.projectExtension.system_default.objective.type =
      payload.objective.type
    state.projectExtension.system_default.objective.function =
      payload.objective.function
    state.projectExtension.system_default.cv = payload.cv
    state.projectExtension.system_default.default_job_extension =
      payload.defaultJobExtension
  },
  addGlobalResource(state, payload) {
    state.projectExtension.global_resources.push({ ...payload })
  },
  loadKolischList(state, kolischList) {
    state.kolischList = kolischList
  },
  setProjectFileName(state, payload) {
    state.addedProject.filename = payload.item.filename
  },
  switchProjectDefault(state, payload) {
    state.enableProjectDefaults = payload
  },
  updateProjectDefaults(state, payload) {
    state.addedProject.project_default.objective.type = payload.objective.type
    state.addedProject.project_default.objective.function =
      payload.objective.function
    state.addedProject.cv = payload.cv
    state.addedProject.project_default.default_job_extension.tr = payload.tr
  },
  addJobExtension(state, payload) {
    state.addedProject.job_extensions.push(payload)
  },
  addResourceExtension(state, payload) {
    state.addedProject.resource_extensions.push(payload)
  },
  addProject(state) {
    state.addedProject.project = state.projectExtension.projects.length
    if (state.addedProject.job_extensions.length == 0) {
      delete state.addedProject.job_extensions
    }
    if (state.addedProject.resource_extensions.length == 0) {
      delete state.addedProject.resource_extensions
    }
    if (!state.enableProjectDefaults) {
      delete state.addedProject.project_default
    }
    state.projectExtension.projects.push(state.addedProject)
    state.addedProject = {
      filename: '',
      project_default: {
        objective: {
          type: '',
          function: '',
        },
        cv: 0,
        default_job_extension: {
          tr: '',
        },
      },
      job_extensions: [],
      resource_extensions: [],
    }
  },
  trunkData(state) {
    if (
      state.projectExtension.system_default.default_job_extension.mode.duration
        .type == 'normal'
    ) {
      delete state.projectExtension.system_default.default_job_extension.mode
        .duration.number
    } else if (
      state.projectExtension.system_default.default_job_extension.mode.duration
        .type == 'fixed'
    ) {
      delete state.projectExtension.system_default.default_job_extension.mode
        .duration.cv
    } else if (
      state.projectExtension.system_default.default_job_extension.mode.duration
        .type == ''
    ) {
      delete state.projectExtension.system_default.default_job_extension.mode
    }
  },
  changeExtensionMessage(state, payload) {
    if (payload == 1) {
      state.extensionSuccessMessage = true
    } else if (payload == 0) {
      state.extensionSuccessMessage = false
    }
  },
  updateSingleObjectiveType(state, payload) {
    state.single_objective.type = payload
  },
  updateSingleObjectiveFunction(state, payload) {
    state.single_objective.function = payload
  },
  addSingleObjective(state) {
    state.projectExtension.system_default.objectives.push(
      state.single_objective
    )
    state.single_objective = {
      type: '',
      function: '',
    }
  },
}

export const actions = {
  async loadKolischList({ commit }) {
    const kolischList = await this.$axios.$get(`/kolisch`)
    commit('loadKolischList', kolischList)
  },
  async sendExtensionData({ commit, state }) {
    commit('trunkData')
    const JSONProjectExtension = JSON.stringify(state.projectExtension)
    try {
      const res = await this.$axios.$post('/create', JSONProjectExtension, {
        headers: { 'Content-Type': 'application/json' },
      })
      commit('changeExtensionMessage', res)
    } catch (err) {
      console.log(err)
    }
  },
}
