import json
import os
import re
import shutil
from pathlib import Path
from subprocess import PIPE, Popen

from flask import Flask, jsonify, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

BASE_MODEL_PATH = Path(__file__).parent.parent.parent / 'data' / 'base_models'
CREATED_MODELS_PATH = Path(__file__).parent.parent.parent / \
    'data' / 'created_model_settings'
EXTENDED_MODELS_PATH = Path(
    __file__).parent.parent.parent / 'data' / 'extended_models'


@app.route('/')
def index():
    """Returns a list of all already generated models. Specifically the .json files used for the extension are saved and delivered by this route."""
    JSONList = BaseProbList(CREATED_MODELS_PATH).create_json_list()
    return jsonify(JSONList)


@app.route('/kolisch')
def kolisch():
    """Returns the list of all base models, that are in the base model directory. Used for the client to choose the base model.
    """
    kolischList = BaseProbList(BASE_MODEL_PATH).create_kolisch_list()
    return jsonify(kolischList)


@app.route('/create', methods=["POST"])
def createJSON():
    content = request.json
    filename = content['name'] + ".json"
    create_model_settings_dir()
    model_extension_file_path = create_settings_data(content)

    extension_success = trigger_extension_process(model_extension_file_path)
    return extension_success


def create_model_settings_dir():
    """Creates the directory for the model settings if it does not exist yet.
    """
    if not CREATED_MODELS_PATH.exists():
        CREATED_MODELS_PATH.mkdir(parents=True)


def create_settings_data(content: dict) -> Path:
    """Creates a directory named after the project name, the json file name, and moves the json file into it with all the necessary base models for the extension process.
    """

    model_name = content['name']
    model_path = Path(CREATED_MODELS_PATH) / model_name
    model_path.mkdir(parents=True, exist_ok=True)
    model_extension_file_path = model_path / f'{model_name}.json'

    with open(model_extension_file_path, 'w') as json_file:
        json.dump(content, json_file)

    base_model_filenames = [
        project["filename"] for project in content['projects']]

    for filename in base_model_filenames:
        base_model_path = BASE_MODEL_PATH / filename
        if not base_model_path.exists():
            raise FileNotFoundError(
                f'The base model {filename} could not be found in the base model directory.')
        else:
            shutil.copy(base_model_path, model_path)

    return model_extension_file_path


def trigger_extension_process(model_extension_file_path: Path) -> bool:
    """Triggers the extension process by calling the gradle wrapper in the hybridpps-model directory."""

    model_dir = model_extension_file_path.parent
    model_dir_copy = Path(__file__).parent.parent / 'hybridpps-model' / \
        'hybridpps.starter' / 'src' / 'main' / 'resources' / model_dir.name

    if not model_dir_copy.exists():
        shutil.copytree(model_dir, model_dir_copy)

    extension_process_gradle_path = Path(
        __file__).parent.parent / 'hybridpps-model' / 'gradlew.bat'
    extend_process_cmd = f":hybridpps.starter:run"
    cwd = Path(__file__).parent.parent / 'hybridpps-model'
    out_path = Path(__file__).parent.parent / \
        'hybridpps-model' / 'hybridpps.starter' / 'extended_models'
    args = f'{model_dir_copy} -o {out_path} --extend'

    result = Popen([extension_process_gradle_path, extend_process_cmd, '--args',
                   args], cwd=cwd, stdout=PIPE, shell=True, encoding='utf-8')
    result_text = result.stdout.read()
    print(result_text)

    extension_success = check_result(result_text)
    if extension_success:
        move_extended_folder()
        shutil.rmtree(model_dir_copy)
        return "1"
    else:
        raise Exception('The extension process failed.')
        return "0"


def check_result(extension_result: str) -> str:
    success_regex = r"(BUILD SUCCESSFUL)"
    fail_regex = r"(BUILD FAILED)"

    successful_match = re.search(success_regex, extension_result)
    fail_match = re.search(fail_regex, extension_result)

    if successful_match:
        return True
    elif fail_match:
        return False
    else:
        print('There was no information about the Build')


def move_extended_folder() -> None:
    """Moves the extended folder from the hybridpps-model to the extended_models folder.
    """
    extended_folder_dir = Path(__file__).parent.parent / \
        'hybridpps-model' / 'hybridpps.starter' / 'extended_models'
    extended_folder_paths = extended_folder_dir.iterdir()

    for extended_folder_path in extended_folder_paths:
        if not extended_folder_path.exists():
            raise FileNotFoundError(
                f'The extended folder could not be found in the hybridpps-model directory.')
        else:
            shutil.move(extended_folder_path, EXTENDED_MODELS_PATH)


class BaseProbList:
    """A list of base Problems that are already available for creation of new models
    """

    def __init__(self, path):
        """Attributes of the BaseProbList

        Args:
            path (str): A string that is the path to the location of all the base problems that should be put into one list
        """
        self.path = path

    def create_json_list(self):
        """creates a a list of all the JSON in the path, the JSON content is also in the list

        Returns:
            list: a full list of the JSON
        """

        dirList = os.listdir(self.path)
        JSONList = []

        for file in dirList:
            if file.endswith('.json'):
                pathToFile = os.path.join(self.path, file)
                with open(pathToFile) as f:
                    JSON = json.load(f)
                JSONList.append(JSON)
        return JSONList

    def create_kolisch_list(self):
        """This function returns a list o the Kolischinstance that are availabe in the problem folder

        Returns:
            kolischList: List of JSON with the filename and path of the Kolischinstances
        """

        dirList = os.listdir(self.path)
        kolischList = []
        fileInfo = {}

        for file in dirList:
            if file.endswith('.DAT'):
                pathToFile = os.path.join(self.path, file)
                fileInfo = {
                    "path": pathToFile,
                    "filename": file
                }
                kolischList.append(fileInfo)
        return kolischList


if __name__ == "__main__":
    app.run(debug=True)
