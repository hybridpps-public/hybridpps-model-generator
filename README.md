# HybridPPS Model Generator

## Description

This project is a model generator for the HybridPPS framework. It generates a model based on a given input model and a given configuration. The input model is a model from the [MPSPLib](http://www.mpsplib.com/) generator. The configuration is a JSON file that specifies the model generation process. The JSON holds the information which values of the production process, job duration, type representative and global resources should be added. It can be further specified for individual jobs. It is kind of a setting file for the extension process in the hybridpps model. Here is the link to the [hybridpps model repository](https://git-st.inf.tu-dresden.de/hybridpps/hybridpps-model).The model generator is implemented as a Java application.

This projects purpose is to give an user interface for extending RCPSP (resource constrained project scheduling problem).

## Requirements

The following things have to be installed on the system to run the model generator:

- [Java 11 JDK](https://learn.microsoft.com/en-us/java/openjdk/download#openjdk-11)
- [Python 3.10](https://www.python.org/downloads/release/python-3100/)
  - includin poetry (can be installed with the following command: `pip install poetry`)
- [node.js](https://nodejs.org/en/download/)
  - including yarn (can be installed with the following command: `npm install --global yarn`)

## Installation

Pull this repo with the following command:

```
git clone git@git-st.inf.tu-dresden.de:hybridpps-public/hybridpps-model-generator.git
```

After the main project is pulled, the submodule from the [hybridpps model repository](https://git-st.inf.tu-dresden.de/hybridpps-public/hybridpps-model) has to be pulled as well. This can be done with the following command:

```
git submodule update --init --recursive
```

Now all the necessary files are pulled and the projects requirements can be installed.

The following command installs the requirements for the backend, the content_manager:

```
poetry install
```

The following command installs the requirements for the frontend, the model_generator, this has to be done in the client folder:

```
cd ./model_generator/client
yarn install
```

The following command builds the necessary files for the java application, the [hybridpps model repository](https://git-st.inf.tu-dresden.de/hybridpps-public/hybridpps-model):

Unix:

```
./model_generator/hybridpps-model/gradle build
```

Windows:

```
./model_generator/hybridpps-model/gradlew build
```

Now all the necessary files are installed and the project can be run.

## Usage

The following command starts the backend, the content_manager:

```
python ./model_generator/content_manager/app.py
```

Check if the backend is running correctly by visiting the url `http://localhost:5000/`. If the backend is running correctly, the browser will show a list of the available models. Check also the endpoint `http://localhost:5000/kolisch` to see if the base models are available.

The following command starts the frontend, the model_generator, this has to be done in the client folder:

```
cd ./model_generator/client
yarn dev
```

Now the model generator can be used in the browser. The url is `http://localhost:3000/`.

### Output

The generated model directories and the base models are located in the data directory. There are three directories, base_models, created_model_settings and extended_models.

The **base_models** directory contains the base models that are used for the extension process. The base models are models similar to those in the [MPSPLib](http://www.mpsplib.com/). The base models are used to generate the extended models. The base models are not changed during the extension process. Their values are taken and extended with stochastic values.

The **created_model_settings** directory contains the settings for the generated models. There are directories that hold the information that has been given to the client interface. The information is inside the <<project_name>>.json file. The information is used to generate the extended models. Next to the json file there are also the base models that are being used to create the extended models. Those are copies from the base_models directory.

The **extended_models** directory contains the generated models. There are directories that hold the generated models. The directories are named after the model name. The model name is the same as in the created_model_settings directory. The name is given in the interface. The generated models are the base models with the extended values. The extended values are the stochastic values that are generated with the given settings.
